﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [HideInInspector]
    public float BulletSpeed = 5; //Determines how fast the bullet travels
    [HideInInspector]
    public float BulletLifetime = 10; //Determines how long the bullet lasts before it disappears


    private Vector3 Direction; //The Direction in which the bullet will travel in the form of a vector
    private float LifetimeCounter = 0f; //Keeps track of the current lifetime of the bullet

    // Start is called before the first frame update
    void Start()
    {
        //Calculate the Direction Vector based on the current Bullet's Z rotation
        Direction = new Vector3(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad),0f);
    }

    // Update is called once per frame
    void Update()
    {
        //Move the bullet using the direction vector
        transform.position += Direction * BulletSpeed * Time.deltaTime;
        //Increment the Lifetime counter of the bullet
        LifetimeCounter += Time.deltaTime;
        //If the lifetime counter is greater than or equal to the bullet's maximum lifetime
        if (LifetimeCounter >= BulletLifetime)
        {
            //Destroy the bullet
            Destroy(gameObject);
        }
    }
}
