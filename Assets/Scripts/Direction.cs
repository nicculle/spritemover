﻿using System;


//Contains a list of valid directions to move the sprite in
//Adding the Flag modifier allows the Directions to be combined, such as combining Up and Right to get Up-Right movement
[Flags]
public enum Direction : byte
{
    None = 0b0,
    Up = 0b1,
    Down = 0b10,
    Left = 0b100,
    Right = 0b1000,
}