﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationQuitter : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //if the Escape Key is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //Quit the Application
            Application.Quit();
        }
    }
}
