﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteController : MonoBehaviour
{
    // Start is called before the first frame update

    //Non-Static Fields
    public float NormalSpeed = 5; //The Speed that the sprite normally travels at
    public float SlowSpeed = 1; //The Speed that the sprite travels when the shift key is held
    ProjectileShooter shooter; //The Script that controls the spawning of bullets

    void Start()
    {
        //Get the Projectile Shooter component
        shooter = GetComponent<ProjectileShooter>();
    }

    // Update is called once per frame
    void Update()
    {
        Direction spriteDirections = Direction.None; //The directions the sprite will be moving in
        //If the shift key is down, then do incremental movement
        if (Input.GetKey(KeyCode.LeftShift))
        {
            SpriteMover.SingleMovement = true; //Set the SpriteMover to use single movements each frame
            SpriteMover.UseDeltaTime = false; //Set the SpriteMover to not use Time.deltaTime so the sprite can move 1 unit each key press
            SpriteMover.Speed = SlowSpeed; //Set the speed to 1 unit
            //If the Up Arrow is pressed
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                //Add the Up direction to spriteDirections
                spriteDirections |= Direction.Up;
            }
            //If the Down Arrow is pressed
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                //Add the Down direction to spriteDirections
                spriteDirections |= Direction.Down;
            }
            //If the Left Arrow is pressed
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                //Add the Left direction to spriteDirections
                spriteDirections |= Direction.Left;
            }
            //If the Right Arrow is pressed
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                //Add the Right direction to spriteDirections
                spriteDirections |= Direction.Right;
            }
        }
        //If not, then do normal movement
        else
        {
            SpriteMover.SingleMovement = false; //Set the SpriteMover to use continous movements
            SpriteMover.UseDeltaTime = true; //Set the SpriteMover to use Time.deltaTime for consistent movement
            SpriteMover.Speed = NormalSpeed; //Set the speed to 5 unit
            //If the Up Arrow is pressed
            if (Input.GetKey(KeyCode.UpArrow))
            {
                //Add the Up direction to spriteDirections
                spriteDirections |= Direction.Up;
            }
            //If the Down Arrow is pressed
            if (Input.GetKey(KeyCode.DownArrow))
            {
                //Add the Down direction to spriteDirections
                spriteDirections |= Direction.Down;
            }
            //If the Left Arrow is pressed
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                //Add the Left direction to spriteDirections
                spriteDirections |= Direction.Left;
            }
            //If the Right Arrow is pressed
            if (Input.GetKey(KeyCode.RightArrow))
            {
                //Add the Right direction to spriteDirections
                spriteDirections |= Direction.Right;
            }
        }
        //Set the SpriveMover's Directions
        SpriteMover.SetDirection(spriteDirections);

        //If the Spacebar is pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Set the sprite's position to the center of the world
            SpriteMover.SetPosition(Vector3.zero);
        }
        //If the P Key is pressed
        if (Input.GetKeyDown(KeyCode.P))
        {
            //Toggle the SpriteMover
            SpriteMover.MoverActive = !SpriteMover.MoverActive;
        }
        //If the Q Key is pressed
        if (Input.GetKeyDown(KeyCode.Q))
        {
            //Disable the gameObject
            gameObject.SetActive(false);
        }
        //If the X key is pressed
        if (Input.GetKeyDown(KeyCode.X))
        {
            //Toggle Smooth Mode
            SpriteMover.SmoothMode = !SpriteMover.SmoothMode;
        }
        //If the C key is pressed
        if (Input.GetKeyDown(KeyCode.C))
        {
            //Toggle Continous mode for the bullet spawner
            shooter.ContinousMode = !shooter.ContinousMode;
        }
    }
}
