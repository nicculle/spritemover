﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour
{

    //Non-Static Variables
    public bool ContinousMode = true; //Determines whether the bullets fire continously or not
    public float BulletSpawnRate = 5; //Determines how many bullets spawn per second
    public float BulletSpeed = 5; //Determines how fast the bullet travels
    public float BulletLifetime = 10; //Determines how long the bullet lasts before it disappears
    public GameObject BulletPrefab; //The Prefab that will be used to spawn the projectiles. Must be set in the inspector


    private float BulletClock = 0F; //A timer to control when the bullets are fired

    // Update is called once per frame
    //Handles the timers and Shooting of the Projectile
    void Update()
    {
        //If Continous mode is on
        if (ContinousMode == true)
        {
            //If the R key is pressed
            if (Input.GetKey(KeyCode.R))
            {
                //Increase the Bullet Clock
                BulletClock += BulletSpawnRate * Time.deltaTime;
                //If the BulletClock is greater than 1
                if (BulletClock > 1)
                {
                    //Reset the clock and fire a bullet
                    BulletClock = 0;
                    ShootProjectile();
                }
            }
        }
        //If continous mode is off
        else
        {
            //If the R key is pressed for a single frame
            if (Input.GetKeyDown(KeyCode.R))
            {
                //Shoot a projectile
                ShootProjectile();
            }
        }
    }

    //Shoots a projectile in the direction the sprite is facing
    public void ShootProjectile()
    {
        //Spawn the Bullet
        Bullet bulletClone = GameObject.Instantiate(BulletPrefab, transform.position, transform.rotation).GetComponent<Bullet>();
        //Set the bullet's Lifetime
        bulletClone.BulletLifetime = BulletLifetime;
        //Set the bullet's speed
        bulletClone.BulletSpeed = BulletSpeed;
    }
}
