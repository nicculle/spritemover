﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMover : MonoBehaviour
{
    //Static Variables
    public static float Speed { get; set; } = 5; //The speed at which the sprite will travel at
    public static bool SingleMovement { get; set; } = false; //When true, only moves the sprite when the SetDirection() function is called and for a single frame only
    public static bool UseDeltaTime { get; set; } = true; //When true, uses Time.deltaTime to have consistent movement even when the FPS is different across different computers
    public static bool MoverActive //Used to get and set whether the script is active or not
    {
        get => MainMover.enabled; //Returns true if the script is active, and false otherwise
        set => MainMover.enabled = value; //Sets whether the script is active or not
    }
    public static bool SmoothMode //Determines whether smooth mode is on or off. When turned on, the Sprite gains much smoother movement
    {
        get => smoothmode; //Return the internal smoothmode state
        set
        {
            //Set the internal smoothmode to the value passed in
            smoothmode = value;
            //If smoothmode is being set to true
            if (smoothmode == true)
            {
                //Reset the Smooth Target to the sprite's current position
                SmoothTarget = MainMover.transform.position;
            }
        }
    }

    //Private static variables
    private static bool smoothmode = false; //Internal variable to store the smooth mode state
    private static SpriteMover MainMover; //Singleton SpriteMover to allow access in static methods
    private static Direction currentDirection = Direction.None; //The direction the sprite is currently traveling in
    private static bool DoOnFrame = false; //Used to run movement for a single frame only. Only used if SingleMovement is turned on
    private static Vector3 SmoothTarget = Vector3.zero; //A target for the sprite to move towards. Only used when Smooth Mode is turned on


    //Non-Static Variables
    public float SmoothSpeed = 7f; //Determines how fast the sprite linear interpolates to the smooth target. Only used when Smooth Mode is on

    // Start is called before the first frame update
    //This sets up the singleton. If it's already set, then destroy the current object
    void Start()
    {
        //If the Singleton is not set
        if (MainMover == null)
        {
            //Set the Singleton to be this object
            MainMover = this;
        }
        else
        {
            //Destroy this object
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    //This controls the movement of the sprite each frame
    //If DoOnFrame is true, the movement only occurs on frames where keys have been pressed
    void Update()
    {
        //If SingleMovement is on and movement is not set to occur on this frame, then return out of the function early
        /*if (SingleMovement && !DoOnFrame)
        {
            return;
        }*/
        Vector3 movement = Vector3.zero; //The amount the sprite will need to move
        float DT = UseDeltaTime ? Time.deltaTime : 1; //Uses Time.deltaTime if UseDeltaTime is set to true

        //Determine how the sprite will move based on the currentDirection

        //If the Up direction is used
        if ((currentDirection & Direction.Up) == Direction.Up)
        {
            //Add upwards movement to the movement vector
            movement += new Vector3(0,Speed * DT,0);
        }
        //If the Down direction is used
        if ((currentDirection & Direction.Down) == Direction.Down)
        {
            //Add downwards movement to the movement vector
            movement += new Vector3(0, -Speed * DT, 0);
        }
        //If the Left direction is used
        if ((currentDirection & Direction.Left) == Direction.Left)
        {
            //Add leftwards movement to the movement vector
            movement += new Vector3(-Speed * DT, 0, 0);
        }
        //If the Right direction is used
        if ((currentDirection & Direction.Right) == Direction.Right)
        {
            //Add rightwards movement to the movement vector
            movement += new Vector3(Speed * DT, 0, 0);
        }

        Quaternion spriteDirection = transform.rotation; //The direction the sprite should be pointing towards
        //If the movement vector has moved
        if (movement != Vector3.zero)
        {
            //Set the sprite direction based off of the movement vector
            Vector3 normal = SmoothMode ? (SmoothTarget + movement - transform.position).normalized : movement.normalized; //Normalized version of the movement vector
            //If Smooth Mode is active, use the Smooth Target relative to the current position, otherwise, just use the movement vector
            float directionAngle = Mathf.Atan2(normal.y, normal.x); //Gets the direction the sprite should be facing by taking the Arc Tangent of the Normal
            spriteDirection = Quaternion.Euler(0, 0, Mathf.Rad2Deg * directionAngle); //Set the final sprite rotation based on the direction angle in degrees
            //If single movement is on, set DoOnFrame to false since this frame has done movement sucessfully
            if (SingleMovement)
            {
                //Set DoOnFrame to false since this frame has done movement sucessfully
                DoOnFrame = false;
            }
        }

        //If Smooth Mode is turned on
        if (SmoothMode)
        {
            //If Single Movement is turned off
            //Or if SingleMovement is on and the current frame is able to do movement
            if (!SingleMovement || (SingleMovement && !DoOnFrame))
            {
                //Add the movement vector to the smooth target
                SmoothTarget += movement;
            }
            //Linearly Interpolate the position using the current position and the smooth target. Set the rotation of the sprite as well
            transform.SetPositionAndRotation(Vector3.Lerp(transform.position, SmoothTarget, SmoothSpeed * Time.deltaTime),spriteDirection);
        }
        //If smooth mode is turned off
        else
        {
            //If Single Movement is turned off
            //Or if SingleMovement is on and the current frame is able to do movement
            if (!SingleMovement || (SingleMovement && !DoOnFrame))
            {
                //Set the sprite's position and rotation using the movement vector
                transform.SetPositionAndRotation(transform.position + movement, spriteDirection);
            }
        }

        
        
    }

    //Sets the direction the sprite will be traveling in
    public static void SetDirection(Direction direction)
    {
        //Change the Direction
        currentDirection = direction;
        //if Single Movement is used, allow the next frame to move the sprite
        if (SingleMovement)
        {
            DoOnFrame = true;
        }
    }

    //Teleports the Sprite to a different location
    public static void SetPosition(Vector3 position)
    {
        //Set the position of the sprite
        MainMover.transform.position = position;
        //If Smooth Mode is turned on
        if (SmoothMode)
        {
            //Set the Smooth Target to the position as well
            SmoothTarget = position;
        }
    }
}
